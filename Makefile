CXX = g++
CXXFLAGS = -std=c++20

.PHONY: all clean

all: bin/main

clean:
	rm -rf bin

bin/main: src/main.cxx
	mkdir -p bin
	$(CXX) $(CXXFLAGS) $^ -o $@
