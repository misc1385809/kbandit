#ifndef KBANDIT_SIMULATE
#define KBANDIT_SIMULATE

#include <algorithm>
#include <chrono>
#include <iostream>
#include "agent.hxx"
#include "kbandit.hxx"

/// @brief Simulate an epsilon-greedy agent for a k-armed bandit.
/// @param kbandit The k-armed bandit to use.
/// @param t The number of trials.
/// @param n The number of steps in each trial.
/// @param e The epsilon value.
void simulate(kbandit& kbandit, std::size_t t, std::size_t n, double e) {
    std::cout << "simulating t=" << t << ", n=" << n << ", e=" << e << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    std::vector<double> avg(n);
    for (std::size_t i = 0; i < t; ++i) {
        agent agent(e);
        for (std::size_t j = 0; j < n; ++j) {
            auto result = agent.pull(kbandit);
            avg[j] = ((avg[j] * i) + result) / (i + 1);
        }
    }

    auto finish = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count();

    auto max = std::ranges::max_element(avg);
    auto index = std::ranges::distance(avg.cbegin(), max);
    std::cout << "max average " << *max << " at index " << index << " (" << duration << "ms)\n" << std::endl;
}

#endif
