#ifndef KBANDIT_KBANDIT
#define KBANDIT_KBANDIT

#include <random>
#include <vector>

/// @brief A k-armed bandit.
class kbandit {
public:
    /// @brief Construct a new kbandit object.
    /// @param arms The number of arms, must be greater than zero.
    kbandit(std::size_t arms) : arms_(arms) {
        for (std::size_t i = 0; i < arms; ++i) {
            levers_.emplace_back(norm_(engine_), 1.0);
        }
    }

    /// @brief Get the number of arms.
    std::size_t arms() const noexcept {
        return arms_;
    }

    /// @brief Pull arm at index and receive reward.
    /// @param index The arm to pull.
    double pull(std::size_t index) {
        return levers_[index](engine_);
    }

private:
    /// @brief A random device.
    static inline std::random_device device_;

    /// @brief A random engine.
    static inline std::mt19937 engine_{device_()};

    /// @brief A normal distribution.
    static inline std::normal_distribution<double> norm_;

    /// @brief The number of arms.
    std::size_t arms_;

    /// @brief The reward distributions of each arm.
    std::vector<std::normal_distribution<double>> levers_;
};

#endif
