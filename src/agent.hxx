#ifndef KBANDIT_AGENT
#define KBANDIT_AGENT

#include <algorithm>
#include <map>
#include <random>
#include "kbandit.hxx"

/// @brief An epsilon-greedy learning agent.
class agent {
public:
    /// @brief Construct a new agent object.
    /// @param epsilon The probability of choosing a random action.
    agent(double epsilon) : epsilon_(epsilon) {
    }

    /// @brief Get the epsilon constant.
    double epsilon() const {
        return epsilon_;
    }

    /// @brief Pull a lever on the k-armed bandit.
    /// @param kbandit The k-armed bandit.
    double pull(kbandit& kbandit) {
        std::size_t action = 0;
        if (uniform_(engine_) < epsilon_) {
            std::uniform_int_distribution<std::size_t> dist(0, kbandit.arms() - 1);
            action = dist(engine_);
        } else {
            auto it = std::ranges::max_element(avg_rewards_, avg_rewards_.value_comp());
            action = std::ranges::distance(avg_rewards_.cbegin(), it);
        }
        auto reward = kbandit.pull(action);
        count_[action] += 1;
        avg_rewards_[action] = avg_rewards_[action] + ((reward - avg_rewards_[action]) / count_[action]);
        return reward;
    }

private:
    /// @brief A random device.
    static inline std::random_device device_;

    /// @brief A random engine to generate random numbers.
    static inline std::mt19937 engine_{device_()};

    /// @brief A uniform real distribution.
    static inline std::uniform_real_distribution<double> uniform_{0, 1};

    /// @brief The probability a random action is chosen.
    double epsilon_;

    /// @brief The map of arm indexes to the count of the arm pulled.
    std::map<std::size_t, std::size_t> count_;

    /// @brief The map of arm indexes to their average reward.
    std::map<std::size_t, double> avg_rewards_;
};

#endif
