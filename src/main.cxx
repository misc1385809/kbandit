#include "kbandit.hxx"
#include "simulate.hxx"

int main(int argc, char** argv) {
    kbandit bandit(10);
    simulate(bandit, 1000, 1000, 0);
    simulate(bandit, 1000, 1000, 0.01);
    simulate(bandit, 1000, 1000, 0.1);
    return 0;
}
